# Debian India Community's homepage.
This is a simple website built using the static site generator
[Hugo](https://gohugo.io/) using the
[Gruvhugo](https://gitlab.com/avron/gruvhugo) theme.

The website is developed on [Debian
Salsa](https://salsa.debian.org/debian-in-team/debian-in-team.pages.debian.net)
and a mirror is maintained for deployment on
[Gitlab](https://gitlab.com/debian-in/debian-in.gitlab.io).

**If you would like to open a issue or merge request please do so on salsa.**
